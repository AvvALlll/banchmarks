#!/bin/bash

echo "RUN PART"

echo "run: python"
python3 rest_measure.py  &

for (( i=1; i<=$#; i+=2 ))
do
    name=$(($i + 1)) &&
    echo "run:" "${!name}"
    ./executable/"${!name}" &
done

wait