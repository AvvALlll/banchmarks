import json
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import kruskal, shapiro, kstest, boxcox, iqr, mannwhitneyu,levene, median_test

def plot_scatter(source_data):      
    for sub_data in source_data:
        xdata = [i for i in range(len(sub_data))]
        plt.scatter(xdata, sub_data)
    plt.ylabel("ms")
    plt.xlabel("time")
    

def plot_hist(compare_info, source_data):
    for sub_data in source_data:
        plt.hist(sub_data, bins=30,  width=0.5, alpha=0.5, density=False)
    plt.ylabel("frequency")        
    plt.xlabel("ms")
    plt.legend([f"{info['legend']}: {len(source_data[i])}, std: {round(np.std(source_data[i]), 3)}" for i, info in  enumerate(compare_info["samples"])], title="n, std")

def compare_single_batch_ordering(compare_info):
    data, server_data, network_data = [], [], []
    for source in compare_info["samples"]:
        with open(source["filename"], "r") as f:
            stat = json.load(f)
            
            part_data = []
            if source["is_single"]:
                batch_size = source["batch_size"]
                print(len(stat["measures"]) // batch_size)
                for i in range(0, len(stat["measures"]), batch_size):
                    part_data.append(round((stat["measures"][i + batch_size - 1]["start_time"] + stat["measures"][i + batch_size - 1]["time"] - stat["measures"][i]["start_time"]) / 10 ** 6, 3))
            else:
                part_data = np.array([round(resp_time["time"] / 10 ** 6, 3) for resp_time in stat["measures"]])
            
            iqr_range = iqr(part_data)
            quantile_25, quantile_75 = np.quantile(part_data, 0.25), np.quantile(part_data, 0.75) 
            part_data = np.array(list(filter(lambda x: quantile_25 - 1.5 * iqr_range < x and quantile_75 + 1.5 * iqr_range > x, part_data)))
            data.append(part_data)
            
            server_part_data = []
            if source["is_single"]:
                batch_size = source["batch_size"]
                for i in range(0, len(stat["measures"]), batch_size):
                    avg_server = 0
                    for j in range(0, batch_size):
                        avg_server += round((int(stat["measures"][i + j]["response"]["outTime"]) - int(stat["measures"][i + j]["response"]["inTime"])) / 10 ** 3, 3)
                    server_part_data.append(avg_server)
            else:
                server_part_data = np.array([round((int(resp_time["response"]["outTime"]) - int(resp_time["response"]["inTime"])) / 10 ** 3, 3) for resp_time in stat["measures"]])
            
            server_iqr_range = iqr(server_part_data)
            quantile_25, quantile_75 = np.quantile(server_part_data, 0.25), np.quantile(server_part_data, 0.75) 
            server_part_data = np.array(list(filter(lambda x: quantile_25 - 1.5 * server_iqr_range < x and quantile_75 + 3 * server_iqr_range > x, server_part_data)))
            server_data.append(server_part_data)
            
            network_part_data = []
            if source["is_single"]:
                batch_size = source["batch_size"]
                for i in range(0, len(stat["measures"]), batch_size):
                    total_latency = round((stat["measures"][i + batch_size - 1]["start_time"] + stat["measures"][i + batch_size - 1]["time"] - stat["measures"][i]["start_time"]) / 10 ** 6, 3)
                    avg_server = 0
                    for j in range(0, batch_size):
                        avg_server += round((int(stat["measures"][i + j]["response"]["outTime"]) - int(stat["measures"][i + j]["response"]["inTime"])) / 10 ** 3, 3)
                    network_part_data.append(total_latency - avg_server)
                print(len(network_part_data))
            else:
                network_part_data = np.array([round(resp_time["time"] / 10 ** 6, 3) - round((int(resp_time["response"]["outTime"]) - int(resp_time["response"]["inTime"])) / 10 ** 3, 3) for resp_time in stat["measures"]])
            
            network_iqr_range = iqr(network_part_data)
            quantile_25, quantile_75 = np.quantile(network_part_data, 0.25), np.quantile(network_part_data, 0.75) 
            network_part_data = np.array(list(filter(lambda x: quantile_25 - 3 * network_iqr_range < x and quantile_75 + 3 * network_iqr_range > x, network_part_data)))
            network_data.append(network_part_data)
    
    if len(server_data) > 1:
        print("DATA:")
        print("sample 1: ", shapiro(data[0]))
        print("sample 2: ", shapiro(data[1]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(*data))
        if len(data) == 2:
            #0 гипотеза, о том, что вебсокет стохастически меньше,  принимается 
            print(mannwhitneyu(*data))
            #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
            print(levene(*data))
        # print(median_test(*data))
        
        print("SERVER DATA")
        #Проверка на нормальность для выборки размером 1000
        print("sample 1: ", shapiro(server_data[0]))
        print("sample 2: ", shapiro(server_data[1]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(*server_data))
        if len(server_data) == 2:
            #0 гипотеза, о том, что вебсокет стохастически меньше,  принимается 
            print(mannwhitneyu(*server_data))
            #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
            print(levene(*server_data))
        # print(median_test(*server_data))
        
        
        print("NETWORK DATA")
        #Проверка на нормальность для выборки размером 1000
        print("sample 1: ", shapiro(network_data[0]))
        print("sample 2: ", shapiro(network_data[1]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(*network_data))
        if len(network_data) == 2:
            #0 гипотеза, о том, что вебсокет стохастически меньше,  принимается 
            print(mannwhitneyu(*network_data))
            #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
            print(levene(*network_data))
        # print(median_test(*network_data))
    
    
    
    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(data)
    #plot frequence hist
    plt.subplot(1, 2, 2)
    plot_hist(compare_info, data)
    plt.savefig(compare_info["out_filename"])
    
    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(server_data)
    #plot frequence hist
    plt.subplot(1, 2, 2)
    plot_hist(compare_info, server_data)
    plt.savefig(compare_info["server_filename"])

    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(network_data)
    #plot frequence hist
    plt.subplot(1, 2, 2)
    plot_hist(compare_info, network_data)
    plt.savefig(compare_info["network_filename"])
            
            

if __name__ == "__main__":
    # compare_single_batch_ordering({
    #     "samples": [{
    #         "filename": "results/batch_comparison/place_single_1.json", 
    #         "legend": "single_1",
    #         "is_single": True,
    #         "batch_size": 1,
    #         },
    #         {
    #         "filename": "results/batch_comparison/place_batch_1.json", 
    #         "legend": "batch_1",
    #         "is_single": False,
    #         "batch_size": 1,
    #     },
    #         {
    #         "filename": "results/batch_comparison/place_single_10.json", 
    #         "legend": "single_10",
    #         "is_single": True,
    #         "batch_size": 10,
    #         },
    #         {
    #         "filename": "results/batch_comparison/place_batch_10.json", 
    #         "legend": "batch_10",
    #         "is_single": False,
    #         "batch_size": 10,
    #     },
    #         {
    #         "filename": "results/batch_comparison/place_single_20.json", 
    #         "legend": "single_20",
    #         "is_single": True,
    #         "batch_size": 20,
    #         },
    #         {
    #         "filename": "results/batch_comparison/place_batch_20.json", 
    #         "legend": "batch_20",
    #         "is_single": False,
    #         "batch_size": 20,
    #     }],
    #     "out_filename": "results/batch_comparison/total_batch_ordering.png",
    #     "server_filename": "results/batch_comparison/total_server_batch_ordering.png",
    #     "network_filename": "results/batch_comparison/total_network_batch_ordering.png",
    # })
    
    compare_single_batch_ordering({
        "samples": [{
            "filename": "results/batch_comparison/place_single_10.json", 
            "legend": "single",
            "is_single": True,
            "batch_size": 10,
            },
            {
            "filename": "results/batch_comparison/place_batch_10.json", 
            "legend": "batch",
            "is_single": False,
            "batch_size": 10,
        }],
        "out_filename": "results/batch_comparison/batch_ordering_10.png",
        "server_filename": "results/batch_comparison/server_batch_ordering_10.png",
        "network_filename": "results/batch_comparison/network_batch_ordering_10.png",
    })