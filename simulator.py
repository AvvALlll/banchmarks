from runner import sock_communication, API, RequestType, Op
import time
import random


get_requests = {API.Tickers: ["/api/v5/market/tickers?instType=SWAP", "/api/v5/market/tickers?instType=SPOT"], 
                API.Ticker: ["/api/v5/market/ticker?instId=BTC-USD-SWAP", "/api/v5/market/ticker?instId=ETH-USDT-SWAP"], 
                API.Orderbook: ["/api/v5/market/books?instId=BTC-USDT&sz=10", "/api/v5/market/books?instId=ETH-USDT&sz=10"],
                API.Full_orderbook: ["/api/v5/market/books-full?instId=BTC-USDT&sz=10", "/api/v5/market/books-full?instId=ETH-USDT&sz=10"],
                API.Candlesticks: ["/api/v5/market/candles?instId=BTC-USDT", "/api/v5/market/candles?instId=ETH-USDT"],
                API.Candlesticks_history: ["/api/v5/market/history-candles?instId=BTC-USDT", "/api/v5/market/history-candles?instId=ETH-USDT"],
                API.Trades: ["/api/v5/market/trades?instId=BTC-USDT", "/api/v5/market/trades?instId=ETH-USDT"],
                API.Trades_history: ["/api/v5/market/history-trades?instId=BTC-USDT", "/api/v5/market/history-trades?instId=ETH-USDT"],
                API.Option_trades_instid: ["/api/v5/market/option/instrument-family-trades?instFamily=BTC-USD"],
                API.Option_trades: ["/api/v5/public/option-trades?instFamily=BTC-USD"],
                API.Total_volume: ["/api/v5/market/platform-24-volume"],
                }

post_requests = {
    API.Place_order: {"query": "/api/v5/trade/order", "args": [
        '{"instId":"BTC-USDT","tdMode":"cash","side":"buy","ordType":"limit","px":"2.15","sz":"2","clOrdId":"btcplacehttp"}',
        '{"instId":"ETH-USDT","tdMode":"cash","side":"buy","ordType":"limit","px":"2.15","sz":"2","clOrdId":"ethplacehttp"}'
    ]},
    API.Cancel_order: {"query": "/api/v5/trade/cancel-order", "args": [
        '{"instId":"BTC-USDT","clOrdId":"btcplacehttp"}',
        '{"instId":"ETH-USDT","clOrdId":"ethplacehttp"}'
        ]},
    API.Amend_order: {"query": "/api/v5/trade/amend-order", "args": [
        '{"instId":"BTC-USDT","clOrdId":"btcplacehttp","newPx":"2.16","newSz":"2"}',
        '{"instId":"ETH-USDT","clOrdId":"ethplacehttp","newPx":"2.16","newSz":"2"}',
    ]}
}

channels = {
    API.Tickers_channel: ['{"channel": "tickers","instId": "BTC-USDT"}'],
    # API.Candlesticks_channel: ['{"channel": "candle1D","instId": "BTC-USDT"}'],
    API.Trades_channel: ['{"channel": "trades","instId": "BTC-USDT"}'],
    API.All_trades_channel: ['{"channel": "trades-all","instId": "BTC-USDT"}'],
    API.Orderbook_channel: ['{"channel": "books","instType":"ANY","instId": "BTC-USDT"}', '{"channel": "books","instType":"ANY","instId": "ETH-USDT"}'],
    API.Order_channel:['{"channel":"orders","instType":"ANY","instId": "BTC-USDT"}', '{"channel":"orders","instType":"ANY","instId": "ETH-USDT"}'],
    API.Option_trades_channel: ['{"channel": "option-trades","instType": "OPTION","instId": "BTC-USDT"}'],
    }

ws_requests = {
    API.Place_order_ws: {"op": Op.Order, "args":[
        '{"instId":"BTC-USDT","tdMode":"cash","side":"buy","ordType":"limit","px":"2.15","sz":"2","clOrdId":"btcplacews"}',
        '{"instId":"ETH-USDT","tdMode":"cash","side":"buy","ordType":"limit","px":"2.15","sz":"2","clOrdId":"ethplacews"}'
        ]},
    API.Cancel_order_ws: {"op": Op.Cancel_order, "args":['{"instId":"BTC-USDT","clOrdId":"btcplacews"}',
                                                         '{"instId":"ETH-USDT","clOrdId":"ethplacews"}']},
    API.Amend_order_ws: {"op": Op.Amend_order, "args":['{"instId":"BTC-USDT","clOrdId":"btcplacews","newPx":"2.16","newSz":"2"}', 
                                                       '{"instId":"ETH-USDT","clOrdId":"ethplacews","newPx":"2.16","newSz":"2"}']}
}

requests = {
    RequestType.Get: get_requests, 
    RequestType.Post: post_requests,
    RequestType.WS: ws_requests,
}

class simulator:
    def __init__(self):
        self.conn = sock_communication(run_python=False)
        
    def start(self):
        self.conn.run()
        for api, args in channels.items():
            for arg in args:
                self.conn.send_sub_request(api, arg)
        start_time = time.time()
        duration = 86400
        
        requests_items = list(requests.items())
        get_requests_list = list(get_requests.items())
        try:
            while time.time() < start_time + duration:
                request_type_idx = random.randint(0, len(requests) - 1)
                request_type, requests_list = requests_items[request_type_idx]
                if len(requests_list) == 0:
                    continue
                
                if request_type == RequestType.Get:
                    for i in range(3):
                        request_idx = random.randrange(0, len(requests_list))
                        api, args = get_requests_list[request_idx]
                        self.conn.send_get_request(api, args[random.randint(0, len(args) - 1)])
                elif request_type == RequestType.WS:
                    args_idx = random.randrange(0, len(ws_requests[API.Place_order_ws]["args"]))
                    self.conn.send_ws_request(API.Place_order_ws, ws_requests[API.Place_order_ws]["op"], ws_requests[API.Place_order_ws]["args"][args_idx])
                    time.sleep(0.05)
                    self.conn.send_ws_request(API.Amend_order_ws, ws_requests[API.Amend_order_ws]["op"], ws_requests[API.Amend_order_ws]["args"][args_idx])
                    time.sleep(0.05)
                    self.conn.send_ws_request(API.Cancel_order_ws, ws_requests[API.Cancel_order_ws]["op"], ws_requests[API.Cancel_order_ws]["args"][args_idx])
                elif request_type == RequestType.Post:
                    args_idx = random.randrange(0, len(post_requests[API.Place_order]["args"]))
                    self.conn.send_post_request(API.Place_order, post_requests[API.Place_order]["query"], post_requests[API.Place_order]["args"][args_idx])
                    time.sleep(0.05)
                    self.conn.send_post_request(API.Amend_order, post_requests[API.Amend_order]["query"], post_requests[API.Amend_order]["args"][args_idx])
                    time.sleep(0.05)
                    self.conn.send_post_request(API.Cancel_order, post_requests[API.Cancel_order]["query"], post_requests[API.Cancel_order]["args"][args_idx])
            self.conn.stop()
        except Exception as e:
            print(e.args)

if __name__ == "__main__":
    sim = simulator()
    sim.start()