#!/bin/bash

if [ ! -d "executable" ]; then
    echo "Creating directory 'executable'..."
    mkdir executable
fi

rm -rf executable/*

i=1
for (( i = 1; i < $#; i++ )); do
    exec_name="$((i + 1))"

    /usr/local/bin/cmake --no-warn-unused-cli -DCMAKE_BUILD_TYPE:STRING=Release \
        -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_C_COMPILER:FILEPATH=/usr/bin/clang-17  \
        -DCMAKE_CXX_COMPILER:FILEPATH=/usr/bin/clang++-17 -S/home/avval/flashflow-framework -B/home/avval/banchmarks/build -G "Unix Makefiles" \
        -DTYPE="${!i}" &&
    /usr/local/bin/cmake --build /home/avval/banchmarks/build --config Release --target all -j 6 --
    mv ./build/flashflow_framework executable/"${!exec_name}"
done

