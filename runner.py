import subprocess
import os
import signal
import time
import sys
from enum import Enum
import socket
import numpy as np
from enum import Enum


class API(Enum):
    # Market data
    # https://www.okx.com/docs-v5/en/#order-book-trading-market-data
    Close = np.uint8(0)
    Tickers = np.uint8(1)
    Ticker = np.uint8(2)
    Orderbook = np.uint8(3)
    Full_orderbook = np.uint8(4)
    Candlesticks = np.uint8(5)
    Candlesticks_history = np.uint8(6)
    Trades = np.uint8(7)
    Trades_history = np.uint8(8)
    Option_trades_instid = np.uint8(9)
    Option_trades = np.uint8(10)
    Total_volume = np.uint8(11)
    Tickers_channel = np.uint8(12)
    Candlesticks_channel = np.uint8(13)
    Trades_channel = np.uint8(14)
    All_trades_channel = np.uint8(15)
    Orderbook_channel = np.uint8(16)
    Option_trades_channel = np.uint8(17)

    # Order routing and trading
    # https://www.okx.com/docs-v5/en/#order-book-trading-trade-post-place-order
    Place_order = np.uint8(18)
    Place_batch_orders = np.uint8(19)
    Cancel_order = np.uint8(20)
    Cancel_batch_orders = np.uint8(21)
    Amend_order = np.uint8(22)
    Amend_batch_orders = np.uint8(23)
    Order_channel = np.uint8(24)
    Place_order_ws = np.uint8(25)
    Place_batch_orders_ws = np.uint8(26)
    Cancel_order_ws = np.uint8(27)
    Cancel_batch_orders_ws = np.uint8(28)
    Amend_order_ws = np.uint8(29)
    Amend_batch_orders_ws = np.uint8(30)

class Op(Enum):
    Subscribe = np.uint8(0)
    Unsubscribe = np.uint8(1)
    Order = np.uint8(2)
    Batch_orders = np.uint8(3)
    Cancel_order = np.uint8(4)
    Batch_cancel_orders = np.uint8(5)
    Amend_order = np.uint8(6)
    Batch_amend_orders = np.uint8(7)
    Mass_cancel = np.uint8(8)


class RequestType(Enum):
    Get = np.uint8(1)
    Subscribe = np.uint8(2)
    Unsubscritbe = np.uint8(3)
    WS = np.uint8(4)
    Post = np.uint8(5)
    Close = np.uint8(6)

class Header:
    msg_type: RequestType
    api_request: API
    size: np.uint16 = np.uint16(0)

    def __init__(self, type=RequestType.Close, api_request: API=API.Close):
        self.msg_type = type
        self.api_request = api_request

    def __bytes__(self):
        return self.msg_type.value.tobytes() + self.api_request.value.tobytes() + self.size.tobytes()

    def from_bytes(self, data):
        offset, cur_member = 0, np.uint8().nbytes
        self.msg_type = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint8)[0]
        offset += cur_member
        cur_member = np.uint8().nbytes
        self.api_request = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint8)[0]
        offset += cur_member
        cur_member = np.uint16().nbytes
        self.api_request = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint16)[0]

    @staticmethod
    def sizeof():
        return np.uint8().nbytes * 2 + np.uint16().nbytes

class GetRequest:
    header: Header
    query_len: np.uint32
    query: bytes

    def __init__(self, header=Header(), query=b''):
        self.header = header
        self.query_len = np.uint32(len(query))
        self.query = query
        self.header.size = np.uint16(self.size())


    def __bytes__(self):
        return bytes(self.header) + self.query_len.tobytes() + self.query
    
    def from_bytes(self, data):
        self.header = Header()
        self.header.from_bytes(data)
        offset, cur_member = Header.sizeof(), np.uint32().nbytes
        self.query_len = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint32)[0]
        offset += cur_member
        self.query = data[offset:offset + self.query_len].decode()
    
    def size(self):
        return Header.sizeof() + np.uint32().nbytes + self.query_len
    
class PostRequest:
    header: Header
    query_len: np.uint32
    data_len: np.uint32
    query: bytes
    data: bytes

    def __init__(self, header=Header(), query=b'', data=b''):
        self.header = header
        self.query_len = np.uint32(len(query))
        self.data_len = np.uint32(len(data))
        self.query = query
        self.data = data
        self.header.size = np.uint16(self.size())


    def __bytes__(self):
        return bytes(self.header) + self.query_len.tobytes() + self.data_len.tobytes() + self.query + self.data
    
    def from_bytes(self, data):
        self.header = Header()
        self.header.from_bytes(data)
        offset, cur_member = Header.sizeof(), np.uint32().nbytes
        self.query_len = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint32)[0]
        offset += cur_member
        self.data_len = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint32)[0]
        offset += cur_member
        self.query = data[offset:offset + self.query_len].decode()
        offset += self.query_len
        self.data = data[offset:offset + self.data_len].decode()
    
    def size(self):
        return Header.sizeof() + np.uint32().nbytes + np.uint32().nbytes + self.query_len + self.data_len

class WsRequest:
    header: Header
    op: Op
    args_len: np.uint32
    args: bytes
    def __init__(self, header, op, args=b''):
        self.header = header
        self.op = op
        self.args_len = np.uint32(len(args))
        self.args = args
        self.header.size = np.uint16(self.size())
        
    def __bytes__(self):
        return bytes(self.header) + self.op.value.tobytes() + self.args_len.tobytes() + self.args
    
    def from_bytes(self, data):
        self.header = Header()
        self.header.from_bytes(data)
        offset, cur_member = Header.sizeof(), np.uint8().nbytes
        self.op = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint8)[0]
        offset += cur_member
        cur_member = np.uint32().nbytes
        self.args_len = np.frombuffer(data[offset:offset + cur_member], dtype=np.uint32)[0]
        offset += cur_member
        self.args = data[offset:offset + self.args_len].decode()
    
    def size(self):
        return Header.sizeof() + np.uint8().nbytes + np.uint32().nbytes + self.args_len

class sock_communication:
    
    def __init__(self, run_python=True):
        self.run_python = run_python
        pass


    def run_all_versions(self):
        self.exec_procs = self.__run_subprocess()
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind(('', 9000))
            sock.listen(1)
            self.connections = []
            for i in range(len(self.exec_procs)):
                conn, addr = sock.accept()
                print("connected: ", addr)
                self.connections.append(conn)
    
    def run(self):
        self.exec_procs = []
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind(('', 9000))
            sock.listen(1)
            self.connections = []
            for i in range(1):
                conn, addr = sock.accept()
                print("connected: ", addr)
                self.connections.append(conn)
        
            
    def stop(self):
        self.send_close()
        for proc in self.exec_procs:
            proc.wait()
            print(proc.returncode)
    
    def run_all_versions_with_high_frequency(self, request_count):
        exec_procs = self.__run_subprocess()
        print(len(exec_procs))
        with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
            sock.bind(('', 9000))
            sock.listen(1)
            self.connections = []
            for i in range(len(exec_procs)):
                conn, addr = sock.accept()
                print("connected: ", addr)
                self.connections.append(conn)

            for i in range(request_count):
                time.sleep(self.timeout)
                for j in range(9):
                    self.send_get_request(API.Ticker, "/api/v5/market/ticker?instId=BTC-USD-SWAP")

        self.stop()

    
    def send_get_request(self, api_request, query):
        msg = bytes(GetRequest(Header(RequestType.Get, api_request), query.encode()))
        for conn in self.connections:
            conn.sendall(msg)
    
    def send_post_request(self, api_request, query, data):
        msg = bytes(PostRequest(Header(RequestType.Post, api_request), query.encode(), data.encode()))
        for conn in self.connections:
            conn.sendall(msg)
            
    def send_sub_request(self, api_request, args):
        msg = bytes(WsRequest(Header(RequestType.Subscribe, api_request), Op.Subscribe, args.encode()))
        for conn in self.connections:
            conn.sendall(msg)
        
    def send_unsub_request(self, api_request, args):
        msg = bytes(WsRequest(Header(RequestType.Unsubscribe, api_request), Op.Unsubscribe, args.encode()))
        for conn in self.connections:
            conn.sendall(msg)

    def send_ws_request(self, api_request, op, args):
        msg = bytes(WsRequest(Header(RequestType.WS, api_request), op, args.encode()))
        for conn in self.connections:
            conn.sendall(msg)

    
    def send_close(self):
        msg = bytes(Header(RequestType.Close))
        for conn in self.connections:
            conn.sendall(msg)


    def __run_subprocess(self):
        exec_procs = [subprocess.Popen(["./executable/" + os.fsdecode(exec)]) for exec in os.listdir("executable")]
        if self.run_python:
            python_proc = subprocess.Popen(["python3", "rest_measure.py"])
            exec_procs.append(python_proc)
        return exec_procs


class signal_communication:

    def __init__(self, request_count, timeout):
        self.request_count = request_count
        self.timeout = timeout

    def run_all_versions(self):
        exec_procs = [subprocess.Popen(["./executable/" + os.fsdecode(exec)]) for exec in os.listdir("executable")]
        # python_proc = subprocess.Popen(["python3", "rest_measure.py"])
        # exec_procs.append(python_proc)

        time.sleep(10)
        for i in range(self.request_count):
            self.make_request(exec_procs)
            time.sleep(self.timeout)

        time.sleep(5)

        self.kill_all_procs(exec_procs)
        
        for proc in exec_procs:
            proc.wait()
            print(proc.returncode)


    def make_request(self, procs: list):
        for i, proc in enumerate(procs):
            print("send request to", i)
            os.kill(proc.pid, signal.SIGUSR1)

    def kill_all_procs(self, procs: list):
        for proc in procs: 
            os.kill(proc.pid, signal.SIGUSR2)


if __name__ == "__main__":
    c = sock_communication(2.5)
    c.run_all_versions_with_high_frequency(100)
#    run_with_low_frequency()