import numpy as np
import json
    import matplotlib.pyplot as plt
import os
from pathlib import Path

import scipy.stats
from scipy.stats import kruskal, shapiro, kstest, boxcox, iqr, mannwhitneyu,levene, median_test
from sklearn.preprocessing import normalize

def plot_hist(filename, result_filename):
    with open(filename, "r") as stat_file:
        flashflow_stat = json.load(stat_file)
        flashflow_time = []
        for i, measures in enumerate(flashflow_stat["measures"]): 
            flashflow_time.append(round(measures["time"] / 10000000, 2))

        unique, counts = np.unique(flashflow_time, return_counts=True)
        print(unique, counts)
        plt.figure(figsize=(16,10))
        plt.bar(counts, unique,  width=.5)
        plt.savefig(result_filename)


class data_info:
    filename: str
    value_name: str

    def __init__(self, filename, value_name):
        self.filename = filename
        self.value_name = value_name


def plot_hist_average(average_values, result_filename):
    averages, stds = [], []
    n_masures = 0
    for value in average_values: 
        with open(value.filename, "r") as stat_file:
            stat = json.load(stat_file)
            averages.append((value.value_name, stat["average"]))
            stds.append(np.std([response["time"] for response in stat["measures"]]))
            n_masures = len(stat["measures"])
    
    names, average_values = zip(*averages)
    plt.figure(figsize=(8,6))
    for i in range(len(names)):
        plt.bar(names[i], average_values[i],  width=.5)
    plt.ylabel("ms")
    plt.ticklabel_format(axis='y', scilimits=(6,6))
    plt.legend([f"{name}: {round(std / pow(10, 6), 2)}" for name, std in zip(names, stds)], title=f"std, n={n_masures}")
    plt.savefig(result_filename)

def plot_hist_high_average(average_values, result_filename):
    averages, stds = [], []
    list_request_n = 9
    n_masures = 0
    for value in average_values:
        with open(value.filename, "r") as stat_file:
            stat = json.load(stat_file)
            tmp_averages = []
            for i in range(0, len(stat["measures"]), list_request_n):
                average = (stat["measures"][i + list_request_n - 1]["start_time"] + stat["measures"][i + list_request_n - 1]["time"] - stat["measures"][i]["start_time"]) / list_request_n
                tmp_averages.append(average)
            averages.append((value.value_name, np.average(tmp_averages)))
            n_masures = len(stat["measures"])

    names, average_values = zip(*averages)
    plt.figure(figsize=(8,6))
    for i in range(len(names)):
        plt.bar(names[i], average_values[i],  width=.5)
    plt.ticklabel_format(axis='y', scilimits=(6,6))
    plt.ylabel("ms")
    # plt.legend([f"{name}: {round(std / pow(10, 7), 2)}" for name, std in zip(names, stds)], title=f"std, n={n_masures}")
    plt.savefig(result_filename)

def plot_freq_hist(info, result_filename):
    data = []
    for filename in info:
        with open(filename.filename, "r") as stat_file:
            stat = json.load(stat_file)
            tmp_data = np.array([round(response["time"] / 10 ** 6, 2)  for response in stat["measures"]])
            iqr_range = iqr(tmp_data)
            quantile_25, quantile_75 = np.quantile(tmp_data, 0.25), np.quantile(tmp_data, 0.75) 
            tmp_data = np.array(list(filter(lambda x: quantile_25 - 1.5 * iqr_range < x and quantile_75 + 3 * iqr_range > x, tmp_data)))
            data.append(tmp_data)

    plt.figure(figsize=(16,10))
    for sub_data in data:
        plt.hist(sub_data, bins=30,  width=0.5, alpha=0.5, density=False)
    plt.ylabel("frequency")        
    plt.xlabel("ms")
    plt.legend([f"{info.value_name}: {len(data[i])}, std: {round(np.std(data[i]), 3)}" for i, info in  enumerate(info)], title="n, std")
    plt.savefig(result_filename)



def plot_scatter(source_data):      
    for sub_data in source_data:
        xdata = [i for i in range(len(sub_data))]
        plt.scatter(xdata, sub_data)
    plt.ylabel("ms")
    plt.xlabel("time")

def load_data_ws_http_1(compare_info):
    data = []
    server_data = []
    network_data = []
    for source in compare_info["samples"]:
        source_folder = Path(source["folder"])
        tmp_data, server_tmp_data, network_tmp_data = [], [], []
        for i in range(len(os.listdir(source_folder))):
            with open(source_folder / (str(i) + "_" + source["filename"]), "r") as stat_file:
                stat = json.load(stat_file)

                part_data = np.array([round(response["time"] / 10 ** 6, 3)  for response in stat["measures"]])
                iqr_range = iqr(part_data)
                quantile_25, quantile_75 = np.quantile(part_data, 0.25), np.quantile(part_data, 0.75) 
                part_data = np.array(list(filter(lambda x: quantile_25 - 1 * iqr_range < x and quantile_75 + 1.5 * iqr_range > x, part_data)))
                tmp_data.extend(part_data)
                # tmp_data = list(map(lambda x: x - tmp_data.mean(), tmp_data))

                #save server handle time
                server_part_data = [(int(response['response']["outTime"]) - int(response['response']["inTime"])) / 10 ** 3 for response in stat["measures"]]
                server_iqr_range = iqr(server_part_data)
                quantile_25, quantile_75 = np.quantile(server_part_data, 0.25), np.quantile(server_part_data, 0.75) 
                server_part_data = np.array(list(filter(lambda x: quantile_25 - 1 * server_iqr_range < x and quantile_75 + 3.5 * server_iqr_range > x, server_part_data)))
                server_tmp_data.extend(server_part_data)

                network_part_data = [round(response["time"] / 10 ** 6, 3) - (int(response['response']["outTime"]) - int(response['response']["inTime"])) / 10 ** 3 for response in stat["measures"]]
                network_iqr_range = iqr(network_part_data)
                quantile_25, quantile_75 = np.quantile(network_part_data, 0.25), np.quantile(network_part_data, 0.75) 
                network_part_data = np.array(list(filter(lambda x: quantile_25 - 1 * network_iqr_range < x and quantile_75 + 3 * network_iqr_range > x, network_part_data)))
                network_tmp_data.extend(network_part_data)

        data.append(tmp_data)
        server_data.append(server_tmp_data)
        network_data.append(network_tmp_data)
        
    compare_ws_http(compare_info, data, network_data, server_data)

def load_data_ws_http_2(compare_info):
    data = []
    server_data = []
    network_data = []
    for source in compare_info["samples"]:
        source_folder = Path(source["folder"])
        with open(source_folder / source["filename"], "r") as stat_file:
            stat = json.load(stat_file)

            part_data = np.array([round(response["time"] / 10 ** 6, 3)  for response in stat["measures"]])
            iqr_range = iqr(part_data)
            quantile_25, quantile_75 = np.quantile(part_data, 0.25), np.quantile(part_data, 0.75) 
            part_data = np.array(list(filter(lambda x: quantile_25 - 1 * iqr_range < x and quantile_75 + 1.5 * iqr_range > x, part_data)))
            data.append(part_data)
            # tmp_data = list(map(lambda x: x - tmp_data.mean(), tmp_data))

            #save server handle time
            server_part_data = [(int(response['response']["outTime"]) - int(response['response']["inTime"])) / 10 ** 3 for response in stat["measures"]]
            server_iqr_range = iqr(server_part_data)
            quantile_25, quantile_75 = np.quantile(server_part_data, 0.25), np.quantile(server_part_data, 0.75) 
            server_part_data = np.array(list(filter(lambda x: quantile_25 - 1 * server_iqr_range < x and quantile_75 + 3.5 * server_iqr_range > x, server_part_data)))
            server_data.append(server_part_data)

            network_part_data = [round(response["time"] / 10 ** 6, 3) - (int(response['response']["outTime"]) - int(response['response']["inTime"])) / 10 ** 3 for response in stat["measures"]]
            network_iqr_range = iqr(network_part_data)
            quantile_25, quantile_75 = np.quantile(network_part_data, 0.25), np.quantile(network_part_data, 0.75) 
            network_part_data = np.array(list(filter(lambda x: quantile_25 - 1 * network_iqr_range < x and quantile_75 + 3 * network_iqr_range > x, network_part_data)))
            network_data.append(network_part_data)
            
    compare_ws_http(compare_info, data, network_data, server_data)

def compare_ws_http(compare_info, data, network_data, server_data):
    
    # for i in range(len(network_data)):
    #     network_data[i] = boxcox(network_data[i], -0.5)
        # network_data[i] = normalize([network_data[i]])[0]
    # print(shapiro(network_data[0]))
    # print(shapiro(network_data[1]))
    # print(kstest(network_data[0], 'norm'))
    # print(kstest(network_data[1], 'norm'))

    if len(network_data) > 1:
        #Проверка на нормальность для выборки размером 1000
        print("sample 1: ", shapiro(network_data[0][:1000]))
        print("sample 2: ", shapiro(network_data[1][:1000]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(*network_data))
        #0 гипотеза, о том, что вебсокет стохастически меньше,  принимается 
        print(mannwhitneyu(*network_data))
        #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
        print(levene(*network_data))
        print(median_test(*network_data))
    
    
    #plot scatter
    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(data)
    #plot frequence hist
    plt.subplot(1, 2, 2)
    for sub_data in data:
        plt.hist(sub_data, bins=30,  width=0.5, alpha=0.5, density=False)
    plt.ylabel("frequency")        
    plt.xlabel("ms")
    plt.legend([f"{info['legend']}: {len(data[i])}, std: {round(np.std(data[i]), 3)}" for i, info in  enumerate(compare_info["samples"])], title="n, std")
    plt.savefig(compare_info["freq_hist"])
    
    #plot server scatter
    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(server_data)
    #plot server frequence hist
    plt.subplot(1, 2, 2)
    for sub_server_data in server_data:
         plt.hist(sub_server_data, bins=100,  width=0.5, alpha=0.5, density=False)
    plt.xlim(0, 3)
    plt.ylabel("frequency")        
    plt.xlabel("ms")         
    plt.legend([f"{info['legend']}: {len(server_data[i])}" for i, info in  enumerate(compare_info["samples"])], title="n")
    plt.savefig(compare_info["freq_server_hist"])
    
    #plot network scatter
    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(network_data)

    #plot network frequence hist
    plt.subplot(1, 2, 2)
    for sub_data in network_data:
        plt.hist(sub_data, bins=30,  width=0.5, alpha=0.5, density=False)
    plt.ylabel("frequency")        
    plt.xlabel("ms")
    plt.legend([f"{info['legend']}: {len(network_data[i])}, std: {round(np.std(network_data[i]), 3)}" for i, info in  enumerate(compare_info["samples"])], title="n, std")
    plt.savefig(compare_info["freq_network_hist"])

def compare_http_versions(compare_info): 
    data = []
    for source in compare_info["samples"]:
        with open(source["source_data"], "r") as stat_file:
            stat = json.load(stat_file)
            tmp_data = np.array([round(response["time"] / 10 ** 6, 3)  for response in stat["measures"]])
            iqr_range = iqr(tmp_data)
            quantile_25, quantile_75 = np.quantile(tmp_data, 0.25), np.quantile(tmp_data, 0.75) 
            tmp_data = np.array(list(filter(lambda x: quantile_25 - 1.5 * iqr_range < x and quantile_75 + 1.5 * iqr_range > x, tmp_data)))
            # tmp_data = list(map(lambda x: x - tmp_data.mean(), tmp_data))
            data.append(tmp_data)

    if len(data) > 1:
        #Проверка на нормальность для выборки размером 1000
        print("sample 1: ", shapiro(data[0]))
        print("sample 2: ", shapiro(data[1]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(data[0], data[1]))
        #0 гипотеза, о том, что вебсокет стохастически равны, принимается 
        print(mannwhitneyu(data[0], data[1]))
        #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
        print(levene(data[0], data[1]))
        
    
    plt.figure(figsize=(16,10))
    plt.subplot(1, 2, 1)
    plot_scatter(data)
    plt.subplot(1, 2, 2)
    for sub_data in data:
        plt.hist(sub_data, bins=30,  width=0.5, alpha=0.5, density=False)
    plt.ylabel("frequency")        
    plt.xlabel("ms")
    plt.legend([f"{info['legend']}: {len(data[i])}, std: {round(np.std(data[i]), 3)}" for i, info in  enumerate(compare_info["samples"])], title="n, std")
    plt.savefig(compare_info["freq_hist"])



if __name__ == "__main__":

    # plotting averages with a stable strategy
    # plot_hist_average([average_value("./results/stable/python_result.json", "other [python]"), 
                    #    average_value("./results/stable/default_ticker.json", "default cpp"), 
                    #    average_value("./results/stable/stable_ticker.json", "my stable")], "./results/stable/stable_average_comparison.png")
    # plotting averages with high frequency requests
    # plot_hist_average([average_value("./results/high_frequency_requests/python_result.json", "other [python]"),  
                    #    average_value("./results/high_frequency_requests/stable_ticker.json", "my")], "./results/high_frequency_requests/high_average_comparison.png")
    # correct averages with high frequency requests
    # plot_hist_high_average([average_value("./results/high_frequency_requests/python_result.json", "other [python]"),  
                    #    average_value("./results/high_frequency_requests/stable_ticker.json", "my")], "./results/high_frequency_requests/correct_high_average_comparison.png")

    #compare http versions
    # compare_http_versions({
    #     "samples": [{"source_data": "./results/http_vs_http2/http1_get.json", "legend": "http1"}, {"source_data": "./results/http_vs_http2/http2_get.json", "legend": "http2"}],
    #     "freq_hist": "./results/http_vs_http2/http_vs_http2_freq.png",
    #     "scatter": "./results/http_vs_http2/http_vs_http2_scatter.png"
    # })
    
    load_data_ws_http_2(
          {"samples" : [{"folder": "./results/http_vs_http2/", "filename": "http1_post.json", "legend": "http1/1"}, 
                        {"folder": "./results/http_vs_http2/", "filename": "http2_post.json", "legend": "http/2"}], 
           "freq_hist": "./results/http_vs_http2/place_http1_vs_http2.png", 
           "freq_server_hist": "./results/http_vs_http2/server_place_http1_vs_http2.png",
           "freq_network_hist": "./results/http_vs_http2/network_place_http1_vs_http2.png"}
    )
    
    load_data_ws_http_2(
          {"samples" : [{"folder": "./data/", "filename": "http1_post.json", "legend": "http1/1"}, 
                        {"folder": "./data/", "filename": "http2_post.json", "legend": "http/2"}], 
           "freq_hist": "./results_charts/place_http1_vs_http2.png", 
           "freq_server_hist": "./results_charts/server_place_http1_vs_http2.png",
           "freq_network_hist": "./results_charts/network_place_http1_vs_http2.png"}
    )
    
    # plot_freq_hist([data_info("./results/http_vs_http2/http11.json", "http1"), data_info("./results/http_vs_http2/http2.json", "http2")], "./results/http_vs_http2/http_vs_http2_1.png")
    # plot_freq_hist([data_info("./results/http_vs_http2/http11_2.json", "http1"), data_info("./results/http_vs_http2/http2_2.json", "http2")], "./results/http_vs_http2/http_vs_http2_2.png")
    # plot_freq_hist([data_info("./results/http_vs_http2/http11_3.json", "http1"), data_info("./results/http_vs_http2/http2_3.json", "http2")], "./results/http_vs_http2/http_vs_http2_3.png")
    
    #test 
    # load_data_ws_http_2(
    #       {"samples" : [{"folder": "./results/routing_comparison/", "filename": "test_place_order_10k.json", "legend": "http"}, 
    #                     {"folder": "./results/routing_comparison/", "filename": "test_place_ws_10k.json", "legend": "ws"}], 
    #        "freq_hist": "./results/routing_comparison/routing_place_10k.png", 
    #        "freq_server_hist": "./results/routing_comparison/routing_place_server_10k.png",
    #        "freq_network_hist": "./results/routing_comparison/routing_place_network_10k.png",
    #        "scatter": "./results/routing_comparison/place_scatter_10k.png", 
    #        "server_scatter": "./results/routing_comparison/place_server_scatter_10k.png",
    #        "network_scatter": "./results/routing_comparison/place_network_scatter_10k.png"}
    # )
    # load_data_ws_http_2(
    #     {"samples" : [{"folder": "./results/routing_comparison/", "filename": "test_cancel_order_10k.json", "legend": "http"}, 
    #                 {"folder": "./results/routing_comparison/", "filename": "test_cancel_ws_10k.json", "legend": "ws"}], 
    #      "freq_hist": "./results/routing_comparison/routing_cancel_10k.png", 
    #      "freq_server_hist": "./results/routing_comparison/routing_cancel_server_10k.png",
    #      "freq_network_hist": "./results/routing_comparison/routing_cancel_network_10k.png",
    #      "scatter": "./results/routing_comparison/cancel_scatter_10k.png", 
    #      "server_scatter": "./results/routing_comparison/cancel_server_scatter_10k.png",
    #      "network_scatter": "./results/routing_comparison/cancel_network_scatter_10k.png"})
    #test
    # load_data_ws_http_1(
    #     {"samples" : [{"folder": "./data/place_order/", "filename":"stable_place_order.json", "legend": "http"}, 
    #                 {"folder": "./data/place_order_ws/", "filename": "stable_place_order_ws.json", "legend": "ws"}], 
    #      "freq_hist": "./results_charts/routing_test_place.png", 
    #      "freq_server_hist": "./results_charts/routing_place_server.png",
    #      "freq_network_hist": "./results_charts/routing_place_network.png",
    #      "scatter": "./results_charts/place_scatter.png", 
    #      "server_scatter": "./results_charts/place_server_scatter.png",
    #      "network_scatter": "./results_charts/place_network_scatter.png"}
    # )
    # load_data_ws_http_1(
    #     {"samples" : [{"folder": "./data/cancel_order/", "filename": "stable_cancel_order.json", "legend": "http"}, 
    #                 {"folder": "./data/cancel_order_ws/", "filename": "stable_cancel_order_ws.json", "legend": "ws"}], 
    #      "freq_hist": "./results_charts/routing_test_cancel.png", 
    #      "freq_server_hist": "./results_charts/routing_cancel_server.png",
    #      "freq_network_hist": "./results_charts/routing_cancel_network.png",
    #      "scatter": "./results_charts/cancel_scatter.png", 
    #      "server_scatter": "./results_charts/cancel_server_scatter.png",
    #      "network_scatter": "./results_charts/cancel_network_scatter.png"}
    # ) 

