
import time
import httpx
import numpy as np
import json
import matplotlib.pyplot as plt
from multiprocessing import Process, Manager, Queue
import utils
import os

class http_request_stat_info:
    start_time: int
    elapsed_time: int
    response: str

    def __init__(self, start_time, elapsed_time, response):
        self.start_time = start_time
        self.elapsed_time = elapsed_time
        self.response = response


def create_request(query, body, API_KEY, API_SECRET_KEY, PASSPHRASE):
    timestamp = utils.get_timestamp()
    str_body = json.dumps(body)
    sign = utils.sign(utils.pre_hash(timestamp, "POST", query, str(str_body), False), API_SECRET_KEY)
    header = utils.get_header(API_KEY, sign, timestamp, PASSPHRASE, "1", False)
    return header, str_body

def send_post(client, request, result_storage, start_time_point, time_delta, requests_counts):
    API_KEY = os.getenv("API_KEY")
    API_SECRET_KEY = os.getenv("SECRET_KEY")
    PASSPHRASE = os.getenv("PASSPHRASE")
    clOrdId = request["place"]["body"]["clOrdId"]
    
    for i in range(requests_counts):
        request["place"]["body"]["clOrdId"] = clOrdId + str(i)
        header, str_body = create_request(request["place"]["query"], request["place"]["body"], API_KEY, API_SECRET_KEY, PASSPHRASE)
        while time.time_ns() < start_time_point + time_delta:
            pass
        
        start = time.time_ns()
        response = client.post(request["place"]["query"], headers=header, data=str_body)
        end = time.time_ns() - start
        res = json.loads(response.text)
        result_storage.put(http_request_stat_info(start, end, res))
        start_time_point += time_delta
        
        request["cancel"]["body"]["clOrdId"] = clOrdId + str(i)
        header, str_body = create_request(request["cancel"]["query"], request["cancel"]["body"], API_KEY, API_SECRET_KEY, PASSPHRASE)
        client.post(request["cancel"]["query"], headers=header, data=str_body)
    print("Done")

def send_post_batch(client, request, result_storage, start_time_point, time_delta, requests_counts):
    API_KEY = os.getenv("API_KEY")
    API_SECRET_KEY = os.getenv("SECRET_KEY")
    PASSPHRASE = os.getenv("PASSPHRASE")
    clOrdId = request["place"]["body"]["clOrdId"]
    
    for i in range(requests_counts):        
        for j in range(20):
            request["place"]["body"]["clOrdId"] = clOrdId + str(j)
            header, str_body = create_request(request["place"]["query"], request["place"]["body"], API_KEY, API_SECRET_KEY, PASSPHRASE)
            start = time.time_ns()
            response = client.post(request["place"]["query"], headers=header, data=str_body)
            end = time.time_ns() - start
            res = json.loads(response.text)
            result_storage.put(http_request_stat_info(start, end, res))
            
            request["cancel"]["body"]["clOrdId"] = clOrdId + str(j)
            header, str_body = create_request(request["cancel"]["query"], request["cancel"]["body"], API_KEY, API_SECRET_KEY, PASSPHRASE)
            client.post(request["cancel"]["query"], headers=header, data=str_body)
        time.sleep(1)
    print("Done")

def send_get(client, query, result_storage, start_time_point, time_delta, requests_counts):
    for i in range(requests_counts):
        while time.time_ns() < start_time_point + time_delta:
            pass

        start = time.time_ns()
        response = client.get(query)
        end = time.time_ns() - start
        res = json.loads(response.text)
        result_storage.put(http_request_stat_info(start, end, res))
        
        start_time_point += time_delta
    print("Done")
    
def compare_http_versions2():
    client1_results, client2_results = Queue(), Queue()
    res1, res2 = [], []
    with httpx.Client(http1=True, timeout=None, base_url="https://www.okx.com") as client1:
        with httpx.Client(http2=True, timeout=None, base_url="https://www.okx.com") as client2:
            start_time_point, time_delta, requests_count= time.time_ns(), 1000 * pow(10, 6), 10
            tasks = [Process(target=send_post,
                            args=(client1, 
                                  {"place" : 
                                      {"query": "/api/v5/trade/order", 
                                       "body" : {"instId":"BTC-USDT","tdMode":"cash","side":"buy","ordType":"limit","px":"2.15","sz":"2","clOrdId":"http1"}
                                       },
                                    "cancel" : 
                                      {"query": "/api/v5/trade/cancel-order", 
                                       "body": {"instId":"BTC-USDT","clOrdId":"http1"}}
                                    },client1_results, start_time_point, time_delta, requests_count)),
                     Process(target=send_post,
                            args=(client2,  {"place" : 
                                      {"query": "/api/v5/trade/order", 
                                       "body" : {"instId":"BTC-USDT","tdMode":"cash","side":"buy","ordType":"limit","px":"2.15","sz":"2","clOrdId":"http2"}
                                       },
                                    "cancel" : 
                                      {"query": "/api/v5/trade/cancel-order", 
                                       "body": {"instId":"BTC-USDT","clOrdId":"http2"}}
                                    }, client2_results, start_time_point, time_delta, requests_count))]
            for task in tasks:
                task.start()
            
            while len(res1) < 100 * 20:
                res1.append(client1_results.get())
                res2.append(client2_results.get())
            for task in tasks:
                task.join()

    start_time_diff = []
    unique_values = []
    for i in range(len(res1)):
        start_time_diff.append(abs(res1[i].start_time - res2[i].start_time))
        if res1[i].response == res2[i].response:
            unique_values.append(i)
    print([res1[i].response for i in range(len(res1))])
    print("Start time difference: ", np.mean(start_time_diff), np.min(start_time_diff), np.max(start_time_diff))
    print("Unique values: ", len(unique_values), " out of: ", len(res1))
    save_stat(res1, "./data/http1_post.json")
    save_stat(res2, "./data/http2_post.json")

def save_stat(responses, file_name = "./data/python_result.json"):
    result_json = {"average" : sum(response.elapsed_time for response in responses) / max(1, len(responses)), "min": min(response.elapsed_time for response in responses), "max": max(response.elapsed_time for response in responses), "measures": []}
    for i in range(0, len(responses)):
        time_responses_dict = {}
        time_responses_dict["start_time"] = responses[i].start_time
        time_responses_dict["time"] = responses[i].elapsed_time
        time_responses_dict["response"] = responses[i].response
        result_json["measures"].append(time_responses_dict)

    with open(file_name, "w") as file:
        json.dump(result_json, file)      

if __name__ == "__main__":
    compare_http_versions2()