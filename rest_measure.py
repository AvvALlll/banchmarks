import requests
import time
import httpx
import asyncio
import numpy as np
import json
import socket
import matplotlib.pyplot as plt
from multiprocessing import Process, Manager, Queue
from threading import Lock, Condition, Thread
import signal
import runner
import utils
import aiohttp
import os

class http_request_stat_info:
    start_time: int
    elapsed_time: int
    response: str

    def __init__(self, start_time, elapsed_time, response):
        self.start_time = start_time
        self.elapsed_time = elapsed_time
        self.response = response

async def httpx_rest():
    responses_info = []
    async with httpx.AsyncClient(http2=True, timeout=None, base_url="https://www.okx.com") as client:
        for i in range(0, 20):
            start = time.time_ns()
            
            response = await client.get("/api/v5/market/ticker?instId=BTC-USD-SWAP")
            end = time.time_ns() - start
            res = json.loads(response.text)
            responses_info.append(http_request_stat_info(start, end, res))
            time.sleep(0.5)
    save_stat(responses_info)


rest_lock = Lock()
rest_cv = Condition(rest_lock)
is_running = True
is_request = False

def request_handler(signum, frame):
    global is_running
    global is_request
    with rest_cv:
        if signal.SIGUSR2 == signum:
            is_running = False
            rest_cv.notify()
            return 

        is_request = True
        rest_cv.notify()



async def httpx_rest_simultaneous():
    global is_request
    signal.signal(signal.SIGUSR1, request_handler)
    signal.signal(signal.SIGUSR2, request_handler)
    responses_info = []
    with rest_cv:
        async with httpx.AsyncClient(http2=True, timeout=None, base_url="https://www.okx.com") as client:
            while is_running:
                rest_cv.wait_for(lambda: is_request or not is_running)
                if not is_running:
                    break
                is_request = False
                start = time.time_ns()
                response = await client.get("/api/v5/market/ticker?instId=BTC-USD-SWAP")
                end = time.time_ns() - start
                res = json.loads(response.text)
                responses_info.append(http_request_stat_info(start, end, res))
    save_stat(responses_info)


async def httpx_sock():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
        sock.connect(('', 9000))
        async with httpx.AsyncClient(http2=True, timeout=None, base_url="https://www.okx.com") as client:
            responses_info = []
            is_running = True
            while is_running:
                data = sock.recv(1024)
                if not data:
                    break
                offset = 0
                while offset < len(data):
                    msg = runner.Header()
                    msg.from_bytes(data[offset:])

                    if msg.msg_type == runner.RequestType.Close.value:
                        offset += runner.Header.sizeof()
                        is_running = False
                        break
                    elif msg.msg_type == runner.RequestType.Get.value:
                        get_request = runner.GetRequest()
                        get_request.from_bytes(data)
                        offset += get_request.size()

                        start = time.time_ns()
                        response = await client.get(get_request.query)
                        end = time.time_ns() - start
                        # res = json.loads(response.text)
                        responses_info.append(http_request_stat_info(start, end, None))
        print("python is done")
        save_stat(responses_info)

def save_stat(responses, file_name = "./data/python_result.json"):
    result_json = {"average" : sum(response.elapsed_time for response in responses) / max(1, len(responses)), "min": min(response.elapsed_time for response in responses), "max": max(response.elapsed_time for response in responses), "measures": []}
    for i in range(0, len(responses)):
        time_responses_dict = {}
        time_responses_dict["start_time"] = responses[i].start_time
        time_responses_dict["time"] = responses[i].elapsed_time
        time_responses_dict["response"] = responses[i].response
        result_json["measures"].append(time_responses_dict)

    with open(file_name, "w") as file:
        json.dump(result_json, file)


        
async def test_aiohttp():
    async with aiohttp.ClientSession() as session:
        for i in range(5):
            start_time = time.time_ns()
            response = await session.get("https://www.okx.com/api/v5/market/ticker?instId=BTC-USD-SWAP")
            end = time.time_ns() - start_time
            print(end)
            time.sleep(30)

if __name__ == "__main__":
    asyncio.run(test_aiohttp())
    # asyncio.run(httpx_sock())