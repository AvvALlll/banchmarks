import json
import numpy as np
import matplotlib.pyplot as plt
from scipy.stats import kruskal, shapiro, kstest, boxcox, iqr, mannwhitneyu,levene, median_test

def plot_scatter(source_data):      
    for sub_data in source_data:
        xdata = [i for i in range(len(sub_data))]
        plt.scatter(xdata, sub_data)
    plt.ylabel("ms")
    plt.xlabel("time")
    

def plot_hist(compare_info, source_data):
    for sub_data in source_data:
        plt.hist(sub_data, bins=30,  width=0.5, alpha=0.5, density=False)
    plt.ylabel("frequency")        
    plt.xlabel("ms")
    plt.legend([f"{legend}: {len(source_data[i])}, std: {round(np.std(source_data[i]), 3)}" for i, legend in  enumerate(compare_info["legends"])], title="n, std")
    


def compare_multi_ws(compare_info):
    raw_data = []
    clOrdid = set()
    with open(compare_info["data_filename"], "r") as stat_file:
        stat = json.load(stat_file)
        tmp_data1, tmp_data2 = [], []
        for value in stat["measures"]:
            if value["response"]["data"][0]["clOrdId"] not in clOrdid:
                clOrdid.add(value["response"]["data"][0]["clOrdId"])
                tmp_data1.append(value)
            if value["response"]["data"][0]["sCode"] == "0":
                tmp_data2.append(value)
        raw_data.append(tmp_data1)
        raw_data.append(tmp_data2)
             
    cnt = 0
    for resp in raw_data[0]:
        if resp["response"]["data"][0]["sCode"] == "0":
            cnt += 1
    print(cnt)
    data, network_data, server_data = [], [], []
    for value_data in raw_data:
        part_data = np.array([round(resp_time["time"] / 10 ** 6, 3) for resp_time in value_data])
        iqr_range = iqr(part_data)
        quantile_25, quantile_75 = np.quantile(part_data, 0.25), np.quantile(part_data, 0.75) 
        part_data = np.array(list(filter(lambda x: quantile_25 - 1.5 * iqr_range < x and quantile_75 + 1.5 * iqr_range > x, part_data)))
        data.append(part_data)
        
        server_part_data = np.array([round((int(resp_time["response"]["outTime"]) - int(resp_time["response"]["inTime"])) / 10 ** 3, 3) for resp_time in value_data])
        server_iqr_range = iqr(server_part_data)
        quantile_25, quantile_75 = np.quantile(server_part_data, 0.25), np.quantile(server_part_data, 0.75) 
        server_part_data = np.array(list(filter(lambda x: quantile_25 - 1.5 * server_iqr_range < x and quantile_75 + 3 * server_iqr_range > x, server_part_data)))
        server_data.append(server_part_data)
            

        network_part_data = np.array([round(resp_time["time"] / 10 ** 6, 3) - round((int(resp_time["response"]["outTime"]) - int(resp_time["response"]["inTime"])) / 10 ** 3, 3) for resp_time in value_data])
        network_iqr_range = iqr(network_part_data)
        quantile_25, quantile_75 = np.quantile(network_part_data, 0.25), np.quantile(network_part_data, 0.75) 
        network_part_data = np.array(list(filter(lambda x: quantile_25 - 3 * network_iqr_range < x and quantile_75 + 3 * network_iqr_range > x, network_part_data)))
        network_data.append(network_part_data)
    
    
    if len(server_data) > 1:
        print("DATA:")
        print("sample 1: ", shapiro(data[0]))
        print("sample 2: ", shapiro(data[1]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(*data))
        if len(data) == 2:
            #0 гипотеза, о том, что вебсокет стохастически меньше,  принимается 
            print(mannwhitneyu(*data))
            #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
            print(levene(*data))
        # print(median_test(*data))
        
        print("SERVER DATA")
        #Проверка на нормальность для выборки размером 1000
        print("sample 1: ", shapiro(server_data[0]))
        print("sample 2: ", shapiro(server_data[1]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(*server_data))
        if len(server_data) == 2:
            #0 гипотеза, о том, что вебсокет стохастически меньше,  принимается 
            print(mannwhitneyu(*server_data))
            #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
            print(levene(*server_data))
        # print(median_test(*server_data))
        
        
        print("NETWORK DATA")
        #Проверка на нормальность для выборки размером 1000
        print("sample 1: ", shapiro(network_data[0]))
        print("sample 2: ", shapiro(network_data[1]))
        #H0 - средние значение во всех группах одинаковы; H1 - Существуют статистически значимые различия между средними значениями хотя бы в одной из групп.
        print(kruskal(*network_data))
        if len(network_data) == 2:
            #0 гипотеза, о том, что вебсокет стохастически меньше,  принимается 
            print(mannwhitneyu(*network_data))
            #если p-value слишком малое, то есть значительные отклонения - https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.levene.html#scipy.stats.levene
            print(levene(*network_data))
        # print(median_test(*network_data))
    

    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(data)
    #plot frequence hist
    plt.subplot(1, 2, 2)
    plot_hist(compare_info, data)
    plt.savefig(compare_info["out_filename"])
    
    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(server_data)
    #plot frequence hist
    plt.subplot(1, 2, 2)
    plot_hist(compare_info, server_data)
    plt.savefig(compare_info["server_filename"])

    fig = plt.figure(figsize=(12, 6))
    plt.subplot(1, 2, 1)
    plot_scatter(network_data)
    #plot frequence hist
    plt.subplot(1, 2, 2)
    plot_hist(compare_info, network_data)
    plt.savefig(compare_info["network_filename"])



if __name__ == "__main__":
    compare_multi_ws(
     {
        "data_filename" : "results/multi_ws/place_ws_multi_4.json",
        "legends" : ["multi", "single"],
        "request_count": 1000,
        "out_filename": "results/multi_ws/cmp_multi_4.png",
        "server_filename": "results/multi_ws/server_cmp_multi_4.png",
        "network_filename": "results/multi_ws/network_cmp_multi_4.png",
     }   
    )